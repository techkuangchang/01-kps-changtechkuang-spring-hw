package com.example.demo.Repository.BookProvider;

import org.apache.ibatis.jdbc.SQL;

public class BookProvider {
    public String findAll() {
        return new SQL(){{
            SELECT("*");
            FROM("tb_books");
        }}.toString();
    }

    public String selectByFilter(String books_id, String title) {
        return new SQL(){{
            SELECT("*");
            FROM("tb_books");

            // Check condition
            if (books_id != null || !books_id.equals(""))
                WHERE("books_id = #{books_id}");
            else if (title != null || !title.equals(""))
                WHERE("title = #{title}");
            else
                WHERE();
        }}.toString();
    }
}
