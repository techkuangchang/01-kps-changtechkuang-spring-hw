package com.example.demo.Repository;

import com.example.demo.Model.Books;
import com.example.demo.Repository.BookProvider.BookProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BooksRepository {

//    @Select("SELECT book_id,title,author,description,thumbnail,category_id,category_id,title FROM tb_books INNER JOIN tb_categories ON category_id = category_id")
//    @Select("Select * FROM tb_books")
//    List<Books> findAll();
    @Insert("INSERT INTO tb_books (books_id, title, author, description, thumbnail, category_id) VALUES (#{books_id}, #{title}, #{author}, #{description}, #{thumbnail}, #{category_id})")
    boolean insert(Books books);

    @SelectProvider(value = BookProvider.class, method = "findAll")
    List<Books> findAll();

    @Select("SELECT * from tb_books WHERE books_id=#{books_id}")
    List<Books> findbyId(int books_id);

    @Update("UPDATE tb_books SET title=#{title}, author=#{author}, description=#{description} WHERE books_id=#{books_id}")
    void update(int books_id, Books books);

    @Delete("DELETE FROM tb_books WEHRE books_id=#{books_id}")
    void delete(int id);
}
