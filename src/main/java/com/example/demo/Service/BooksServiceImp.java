package com.example.demo.Service;

import com.example.demo.Model.Books;
import com.example.demo.Repository.BooksRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BooksServiceImp implements BooksService {

    private BooksRepository booksRepository;

    @Autowired
    public void setBooksRepository(BooksRepository booksRepository) {
        this.booksRepository = booksRepository;
    }

    @Override
    public Books insert(Books books) {
        boolean isInserted = booksRepository.insert(books);
        if(isInserted)
            return books;
        else
            return null;
    }

    @Override
    public List<Books> findAll() {
        return booksRepository.findAll();
    }

    @Override
    public List<Books> findbyId(int books_id) {
        return booksRepository.findbyId(books_id);
    }

    @Override
    public void update(int books_id, Books books) {
        booksRepository.update(books_id,books);
    }

    @Override
    public void delete(int books_id) {
        booksRepository.delete(books_id);
    }
}
