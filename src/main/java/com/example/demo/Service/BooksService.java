package com.example.demo.Service;

import com.example.demo.Model.Books;

import java.util.List;

public interface BooksService {
    List<Books> findAll();
    Books insert (Books books);
    List<Books> findbyId(int books_id);
    void update (int books_id, Books books);
    void delete (int books_id);
}
