package com.example.demo.Controller;

import com.example.demo.Controller.Reponse.ApiResponse;
import com.example.demo.Model.Books;
import com.example.demo.Service.BooksService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@RestController
public class BooksRESTController {
    private BooksService booksService;

    @Autowired
    public void setBooksService(BooksService booksService) {
        this.booksService = booksService;
    }

    @PostMapping("/books")
    public ResponseEntity<ApiResponse> insert(@RequestBody Books books){
        ApiResponse<Books> response = new ApiResponse<>();

        ModelMapper mapper = new ModelMapper();
        Books book = mapper.map(books,Books.class);

        Books res = booksService.insert(book);

        Books res2 = mapper.map(res,Books.class);
        response.setMessage("You have added book successfully");
        response.setData(res2);
        response.setStatus(HttpStatus.OK);
        response.setTime(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(response);
    }

    @GetMapping("/books")
    public ResponseEntity<ApiResponse<List<Books>>> findAll(){
        ModelMapper mapper = new ModelMapper();
        ApiResponse<List<Books>> response=new ApiResponse<>();
        List<Books> booksList = booksService.findAll();
        List<Books> books = new ArrayList<>();
        for (Books books1 : booksList){
            books.add(mapper.map(books1,Books.class));
        }
        response.setMessage("Have already find all book");
        response.setData(books);
        response.setStatus(HttpStatus.OK);
        response.setTime(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(response);
    }

    @GetMapping("/books/{books_id}")
    public ResponseEntity<ApiResponse<List<Books>>> findbyId(@PathVariable int books_id,@RequestBody Books books){
        ModelMapper mapper = new ModelMapper();
        ApiResponse<List<Books>> response=new ApiResponse<>();
        List<Books> booksList = booksService.findbyId(books_id);
        List<Books> book = new ArrayList<>();
        for (Books books1 : booksList){
            book.add(mapper.map(books1,Books.class));
        }
        response.setMessage("Have already find all book");
        response.setData(book);
        response.setStatus(HttpStatus.OK);
        response.setTime(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(response);
    }

    @PutMapping("/{books_id}")
    public ResponseEntity<String> update(@PathVariable int books_id,@RequestBody Books books){
        booksService.update(books_id,books);
        return ResponseEntity.ok("Updated");
    }

    @DeleteMapping("{books_id}")
    public ResponseEntity<String> delete(@PathVariable int books_id,@RequestBody Books books){
        booksService.delete(books_id);
        return ResponseEntity.ok("Delete Successfuly");
    }
}
